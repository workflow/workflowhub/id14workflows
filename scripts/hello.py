from ewokscore.task import Task

class Hello(Task, input_names=["name"], output_names=["response"]):
	def run(self):
		self.outputs.response = f"Hello {self.inputs.name}"
